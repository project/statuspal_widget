<?php

namespace Drupal\statuspal_widget\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Statuspal notification status block.
 *
 * @Block(
 *   id = "statuspal_block_button",
 *   admin_label = @Translation("StatusPal notification button"),
 *   category = @Translation("Custom")
 * )
 */
class StatusPalButton extends BlockBase {

  /**
   *
   */
  public function build() {
    $render_array[] = [
      // Create the button element.
      'widget_btn' => [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#value' => $this->t('Check statuses'),
        '#attributes' => [
          'id' => ['statuspal-button'],
        ],
      ],
      '#attached' => [
        'library' => [
          'statuspal_widget/statuspal-widget',
        ],
      ],
    ];

    return $render_array;
  }

}
