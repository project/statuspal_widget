<?php

namespace Drupal\statuspal_widget\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Statuspal notification status block.
 *
 * @Block(
 *   id = "statuspal_block_container",
 *   admin_label = @Translation("StatusPal notification container"),
 *   category = @Translation("Custom")
 * )
 */
class StatusPalContainer extends BlockBase {

  /**
   *
   */
  public function build() {
    $render_array[] = [
      // Create the data container element.
      'widget_data_container' => [
        '#type' => 'container',
        '#attributes' => [
          'id' => ['statuspal-container'],
        ],
      ],
      '#attached' => [
        'library' => [
          'statuspal_widget/statuspal-widget',
        ],
      ],
    ];

    return $render_array;
  }

}
