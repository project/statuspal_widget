<?php

namespace Drupal\statuspal_widget\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the statuspal_widget settings form.
 *
 * @package Drupal\statuspal_widget\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['statuspal_widget.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'statuspal_widget_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('statuspal_widget.settings');

    // $form['use_drupal_blocks'] = [
    //   '#type' => 'checkbox',
    //   '#title' => $this->t('Use Drupal blocks'),
    //   '#default_value' => $config->get('use_drupal_blocks', TRUE),
    //   '#description' => $this->t('This will provide you with 2 Drupal blocks that you can place in your theme. One for the button with status indicator, and one for the message container. Both blocks needs to be placed.'),
    // ];
    // Fieldset wrapper for below themeing elements
    // $form['theming_fieldset'] = [
    //   '#type' => 'fieldset',
    //   '#title' => $this->t('Custom themeing settings'),
    //   '#states' => [
    //     'visible' => [
    //       ':input[name="use_drupal_blocks"]' => ['checked' => FALSE],
    //     ],
    //   ],
    // ];.
    // Add fields for selectors if user is not using blocks.
    // $form['theming_fieldset']['widget_button_selector'] = [
    //   '#type' => 'textfield',
    //   '#title' => $this->t('Status button id selector'),
    //   '#default_value' => $config->get('widget_button_selector'),
    //   '#states' => [
    //     'visible' => [
    //       ':input[name="use_drupal_blocks"]' => ['checked' => FALSE],
    //     ],
    //   ],
    //   '#description' => $this->t('The id of the widget button, i.e. the html element you click to check status. Should ideally be a html button.'),
    // ];.
    // $form['theming_fieldset']['widget_container_selector'] = [
    //   '#type' => 'textfield',
    //   '#title' => $this->t('Status container id selector'),
    //   '#default_value' => $config->get('widget_container_selector'),
    //   '#states' => [
    //     'visible' => [
    //       ':input[name="use_drupal_blocks"]' => ['checked' => FALSE],
    //     ],
    //   ],
    //   '#description' => $this->t('The id of the container are, i.e. the html element visible messages will appear in. Typically a div.'),
    // ];
    $form['statuspal_endpoint'] = [
      '#type' => 'url',
      '#title' => $this->t('Statuspal Endpoint URL'),
      '#default_value' => $config->get('statuspal_endpoint'),
      '#description' => $this->t('Your endpoint from StatuPal, like https://statuspal.eu/api/v2/status_pages/YOUR_ID/summary'),
      '#required' => TRUE,
    ];

    $form['container_position'] = [
      '#type' => 'radios',
      '#title' => $this->t('Placement of messages'),
      '#default_value' => $config->get('container_position'),
      '#options' => [
        'position-top-right' => $this->t('Top right'),
        'position-bottom-right' => $this->t('Bottom right'),
        'position-middle' => $this->t('Middle'),
      ],
      '#description' => $this->t('Placement of messages'),
      '#required' => TRUE,
    ];

    $form['development_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Development mode'),
      '#default_value' => $config->get('development_mode', FALSE),
      '#description' => $this->t('Bypass the API and load test data, marked as unread. Useful for themeing and testing.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // $selectors = [
    //   $form_state->getValue('widget_button_selector'),
    //   $form_state->getValue('widget_container_selector')
    // ];
    // Foreach ($selectors as $key => $selector) {
    //   // Check if the selector contains spaces.
    //   if (strpos($selector, ' ') !== false) {
    //     $form_state->setErrorByName($key === 0 ? 'widget_container_selector' : 'widget_button_selector', $this->t('The selector should not contain spaces.'));
    //   }
    //   // Use a regular expression to validate as a valid HTML selector.
    //   if (!preg_match('/^[a-zA-Z][\w-]*$/', $selector)) {
    //     $form_state->setErrorByName($key === 0 ? 'widget_container_selector' : 'widget_button_selector', $this->t('Invalid HTML selector format.'));
    //   }
    // }.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('statuspal_widget.settings');

    // Store the values.
    $config
      // ->set('use_drupal_blocks', $form_state->getValue('use_drupal_blocks'))
      // ->set('widget_button_selector', $form_state->getValue('widget_button_selector'))
      // ->set('widget_container_selector', $form_state->getValue('widget_container_selector'))
      ->set('statuspal_endpoint', $form_state->getValue('statuspal_endpoint'))
      ->set('container_position', $form_state->getValue('container_position'))
      ->set('development_mode', $form_state->getValue('development_mode'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
