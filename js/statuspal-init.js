(function statusPalInitializeWidget(drupalSettings) {
  // Check if the statuspal_widget settings is set in drupalSettings
  if (drupalSettings && drupalSettings.statuspalWidget) {
    const config = drupalSettings.statuspalWidget;
    document.addEventListener('DOMContentLoaded', () => {
      // eslint-disable-next-line no-undef
      initializeCustomStatuspalWidget(config);
    });
  }
})(drupalSettings);
