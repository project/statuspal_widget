# Statuspal widget

Provides a widget with ongoing incidents from incident service 
[StatusPal](https://www.statuspal.io/). The module uses the Statuspal 
[summary endpoint](https://www.statuspal.io/api-docs#tag/Summary), and will show 
a separatemessage for an incident, a maintainance and / or an upcoming maintainance.

The module provides two blocs that can be placed in your theme. One for the
button element that toggles messages, if there are any, and one for the message
container. Both blocks needs to be placed for it to work. The button shows a
counter with number of ongoing incidents, and also wether the user has read
them or not.

Data is stored in javascript sessionStorage.

Some basic styling is provided, but you would likely want to override this in
your theme.

A setting for development mode is included, toggling this will replace the data
from the service with included static test data, and the status of incidents
will be set to unread.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/statuspal_widget).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/statuspal_widget).


## Table of contents

- Requirements
- Installation
- Configuration
- FAQ
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to Administration » Configuration » Web Services » Statuspal Widget
1. Desired placement of your messages
1. Toggle development mode as you need


## FAQ
**Q: How to get endpoint URL?**

**A:** Sign up on [statuspal](https://statuspal.io/registrations/new) website.


## Maintainers

Vegard A. Johansen - [vegardjo](https://www.drupal.org/u/vegardjo)
Martin Mikkelsen [mamik](https://www.drupal.org/u/mamik)
